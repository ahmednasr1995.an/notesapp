package com.example.mynotessql;


import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import static android.view.View.INVISIBLE;
import static android.view.View.VISIBLE;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
EditText titleET;
EditText descET;
RecyclerView recyclerView;
Button save;
Switch deleteSwitch;
NotesDB notesDB;



    ArrayList<NotesModel> notes = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        notesDB =  new NotesDB(this);
        initViews();

    }

    @Override
    protected void onStart() {
        super.onStart();
        notes = notesDB.getNotes();
        implementRV(notes);

    }

    @Override
    protected void onResume() {
        super.onResume();
        notes = notesDB.getNotes();
        implementRV(notes);
    }

    private void implementRV(ArrayList<NotesModel> notes) {

        recyclerView.setAdapter(new NotesAdapter(notes,this));
        recyclerView.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false));
    }

    private void initViews() {
         titleET = findViewById(R.id.titleET);
         descET= findViewById(R.id.descET);
         recyclerView = findViewById(R.id.recyclerView);
         save = findViewById(R.id.saveBut);
         deleteSwitch = findViewById(R.id.deleteSwitch);
         notes = notesDB.getNotes();

         save.setOnClickListener(this);
         deleteSwitch.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.saveBut:
                notesDB.addNote(new NotesModel(titleET.getText().toString(),descET.getText().toString()));
                notes = notesDB.getNotes();
                implementRV(notes);
                titleET.setText("");
                descET.setText("");
                break;
            case R.id.deleteSwitch:
                notes = notesDB.getNotes();
                implementRV(notes);
                break;
        }
    }

}
