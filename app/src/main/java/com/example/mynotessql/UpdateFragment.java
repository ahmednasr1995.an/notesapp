package com.example.mynotessql;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class UpdateFragment extends DialogFragment implements View.OnClickListener {
    View view;
    EditText title;
    EditText desc;
    Button update;
    String id = "";
    NotesDB notesDB;
    RecyclerView recyclerView;

    public UpdateFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_update, container, false);
        notesDB =  new NotesDB(getContext());
        initViews();



        return view;
    }

    private void implementRV(ArrayList<NotesModel> notes) {

        recyclerView.setAdapter(new NotesAdapter(notes,getContext()));
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false));
    }

    private void initViews() {
        title = view.findViewById(R.id.titleETFragment);
        desc = view.findViewById(R.id.descETFragment);
        update = view.findViewById(R.id.updateBut);
        recyclerView =  ((MainActivity)getContext()).findViewById(R.id.recyclerView);

        id = getArguments().getString("id");
        title.setText(getArguments().getString("title"));
        desc.setText(getArguments().getString("Description"));

        update.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        notesDB.updateNote(id,title.getText().toString(),desc.getText().toString());
        implementRV(notesDB.getNotes());
        dismiss();
    }


    @Override
    public void onStart() {
        super.onStart();
        getDialog().getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.WRAP_CONTENT);
    }
}
