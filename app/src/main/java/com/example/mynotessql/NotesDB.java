package com.example.mynotessql;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

import java.util.ArrayList;


public class NotesDB extends SQLiteOpenHelper {

    static final String DB_NAME = "My Database";
    static final String TABLE_NAME = "MyNotes";
    static final String KEY_TITLE = "title";
    static final String KEY_DESC = "description";
    static final String KEY_ID = "id";
    static final int DB_VERSION = 1;

    public NotesDB(@Nullable Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
    String createTable = "create Table " + TABLE_NAME + "(" + KEY_ID + " integer primary key, "
                + KEY_TITLE + " varchar(30), " + KEY_DESC + " integer )";

    db.execSQL(createTable);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    String deleteQuery = "DROP TABLE IF Exists " + TABLE_NAME;
    db.execSQL(deleteQuery);
    onCreate(db);

    }


    public void addNote(NotesModel notesModel){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values =  new ContentValues();
        values.put(KEY_TITLE,notesModel.getTitle());
        values.put(KEY_DESC,notesModel.getDescription());
        db.insert(TABLE_NAME,null,values);


        String selectQuery = "SELECT * FROM " + TABLE_NAME  ;
        SQLiteDatabase db1 = this.getReadableDatabase();
        Cursor cursor = db1.rawQuery(selectQuery,null);
        cursor.moveToLast();
        String storedId = cursor.getString(cursor.getColumnIndex(KEY_ID));
        notesModel.setId(storedId);

    }

    public ArrayList<NotesModel> getNotes(){
        ArrayList<NotesModel> notesModelArrayList = new ArrayList<>();
        String selectQuery = "SELECT * FROM " + TABLE_NAME;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery,null);
        if(cursor.moveToFirst()){
            do{
                String storedTitle = cursor.getString(cursor.getColumnIndex(KEY_TITLE));
                String storedId = cursor.getString(cursor.getColumnIndex(KEY_ID));
                String storedDesc = cursor.getString(cursor.getColumnIndex(KEY_DESC));
                NotesModel notesModel = new NotesModel(storedTitle,storedDesc);
                notesModel.setId(storedId);
                notesModelArrayList.add(notesModel);
            } while (cursor.moveToNext());
        }
        return notesModelArrayList;
    }

    void updateNote(String id, String title, String desc){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values =  new ContentValues();
        values.put(KEY_TITLE,title);
        values.put(KEY_DESC,desc);
        db.update(TABLE_NAME, values, KEY_ID + " = " + id, null);


    }

    void deleteNote(String id){
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_NAME, KEY_ID + " = " + id, null);


    }
}
