package com.example.mynotessql;

import android.view.View;

public class NotesModel {
    String id;
    String title;
    String description;
    public static final int  INVISIBLE = View.INVISIBLE;
    public static final int  VISIBLE = View.VISIBLE;

    public NotesModel() {
    }

    public NotesModel( String title, String description) {
        this.title = title;
        this.description = description;
    }


    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }
}
