package com.example.mynotessql;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import static android.view.View.INVISIBLE;
import static android.view.View.VISIBLE;

public class NotesAdapter extends RecyclerView.Adapter<NotesAdapter.MyViewHolder> {
    ArrayList<NotesModel> notes;
    Context context;
    RecyclerView recyclerView;

    public NotesAdapter(ArrayList<NotesModel> notes,Context context) {
        this.notes = notes;
        this.context = context;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.notesview,parent,false);
        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {
        final Switch delSwitch = ((MainActivity)context).findViewById(R.id.deleteSwitch);
        if(delSwitch.isChecked()){
            holder.deleteBut.setVisibility(VISIBLE);

        }else{
            holder.deleteBut.setVisibility(INVISIBLE);
        }

        holder.title.setText(notes.get(position).getTitle());
        holder.desc.setText(notes.get(position).getDescription());
        holder.deleteBut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NotesDB notesDB  = new NotesDB(context);
                notesDB.deleteNote(notes.get(position).getId());
                Toast.makeText(context,"Deleted",Toast.LENGTH_LONG).show();
                recyclerView = ((MainActivity)context).findViewById(R.id.recyclerView);
                recyclerView.setAdapter(new NotesAdapter(notesDB.getNotes(),context));
                recyclerView.setLayoutManager(new LinearLayoutManager(context,LinearLayoutManager.VERTICAL,false));
            }
        });
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(v.getId() != R.id.deleteNote){
                    UpdateFragment updateFragment = new UpdateFragment();
                    Bundle bundle = new Bundle();
                    bundle.putString("id",notes.get(position).getId());
                    bundle.putString("title",notes.get(position).getTitle());
                    bundle.putString("Description",notes.get(position).getDescription());
                    updateFragment.setArguments(bundle);
                    updateFragment.show(((MainActivity)context).getSupportFragmentManager(),"");
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return notes.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView title;
        TextView desc;
        ImageView deleteBut;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.viewTitle);
            desc = itemView.findViewById(R.id.viewDesc);
            deleteBut = itemView.findViewById(R.id.deleteNote);


        }
    }
}



